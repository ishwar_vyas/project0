// let dayNum= prompt("Enter the day number")
// switch(dayNum) {
//       case "0":
//         alert("Today is Monday");
//         break;
//         case "1":
//             alert("Today is Tuesday");
//             break;
//             case "2":
//         alert("Today is Wednesday");
//         break;
//         case "3":
//         alert("Today is Thursday");
//         break;
//         case "4":
//         alert("Today is Friday");
//         break;
//         case "5":
//         alert("Today is Saturday");
//         break;
//         case "6":
//         alert("Today is Sunday");
//         break;
//       default:
//         alert("Enter number between 0-6");
//     }

//to convert string to int use parseInt


//using higher order func convert each element of array into cube

let arr1=[1,2,3,4];

let cube=arr1.map(item=>item*item*item);

console.group(cube)

//list the element which are odd

let arr2=[1,2,3,4,5,6,7];
let odd=arr2.filter(item2=>item2%2!==0);
console.log(odd)

//product of elements of an array

let arr3=[1,2,3,4];
let product=arr3.reduce((sum,item)=>sum*item)
console.log(product)

//list the array elements which are more than given length

let arr4=["one","two","three","four","five"];
let arr5=arr4.filter(item=>item.length>3);
console.log(arr5)